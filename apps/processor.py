"""
This script contains functionalities to select prompts depending on whether a description
should be corrected or not
"""

import pandas as pd
import re
from jls import jls, Context, DF, Any


@jls.processor(id='prompt_selection')
def prompt_selection_processor(descriptions_correction_matches: DF[Any],
                               prompts: DF[Any],
                               context: Context):
    """

    Args:
        descriptions_correction_matches: a dataframe on only one column, where each row represents
        whether a previous description matches the correction regex: in other words should be corrected
        prompts: a dataframe containing the 2 candidate prompts
        context: the usual Malevich context

    Returns: the first prompt if all the rows are False, otherwise the 2nd
    """
    if prompts.shape != (2, 1):
        raise ValueError(f"The prompts dataframe is assumed to be of shape: {(2, 1)}\n"
                         f"Found: {prompts.shape}")

    p1, p2 = prompts.iloc[:, 0]

    descriptions_correction_matches.fillna('', inplace=True)
    correction_matches = descriptions_correction_matches.iloc[:, 0]
    print(correction_matches)

    # if none of the matches if True, it means none of the descriptions should be corrected,
    # and the first prompt should be selected
    p = p1 if all([cm is None or len(cm) == 0 for cm in correction_matches]) else p2

    return pd.DataFrame(data={"prompt": [p]})
