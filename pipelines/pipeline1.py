# let's assume the correction regex pattern is **.**
import random
import string
import os

import jls_utils as ju
import pandas as pd

from typing import Union
from pathlib import Path

correction_regex_pattern = r'\*\*.*\*\*'
script_dir = os.path.dirname(os.path.realpath(__file__))
data_folder = os.path.join(Path(script_dir).parent, 'data')


def create_description_examples():
    correct_descriptions = ['desc1', 'desc2', 'desc3']
    incorrect_descriptions = ['**xx**', 'I am an **uncorrected** description', '**-**']
    p1 = 'prompt1'
    p2 = 'prompt2'

    correct_dataframe = pd.DataFrame(data={"prompt": correct_descriptions})
    incorrect_df1 = pd.DataFrame(data={"prompt": correct_descriptions + incorrect_descriptions[:1]})
    incorrect_df2 = pd.DataFrame(data={"prompt": correct_descriptions + incorrect_descriptions[1:2]})
    incorrect_df3 = pd.DataFrame(data={"prompt": correct_descriptions + incorrect_descriptions[2:]})

    dfs = [correct_dataframe, incorrect_df1, incorrect_df2, incorrect_df3]

    for index, d in enumerate(dfs):
        d.to_csv(os.path.join(data_folder, f'dataframe_{index}.csv'), index=False)


def random_str(str_len: int) -> str:
    return "".join([random.choice(string.ascii_lowercase) for _ in range(str_len)])


def match_prompt_selection_pipeline(dataframe_path: Union[Path, str],
                            prompts_df_path: Union[Path, str]):
    ju.update_core_credentials(username=random_str(str_len=20), password='pass_0')

    try:
        ju.delete_user()
    except:
        pass
    finally:
        ju.set_host_port('http://localhost:8080')
        ju.create_user()

    basic_match_config = {"pattern": correction_regex_pattern, "join_char": "----"}
    # create the collection from the dataframe
    pattern_match_collection = ju.create_collection_from_file(dataframe_path)

    ju.create_app(
        # the app_id is present in the processor file of the utility/match_pattern directory
        app_id='match_pattern_app',
        processor_id='pattern_match_processor',
        input_id='match_pattern_input',
        image_ref='utility',
        app_cfg=basic_match_config
    )

    # create the application of prompt_selection
    ju.create_app(
        app_id='prompt_selection_app',
        processor_id='prompt_selection',
        input_id='select_two_prompts_collection',
        image_ref='prompt_selection',
        app_cfg={}
    )

    two_prompts_collection = ju.create_collection_from_file(prompts_df_path)

    ju.create_task(task_id='task',
                   app_id='prompt_selection_app',
                   apps_depends=['match_pattern_app'])

    # create the collection
    task_configuration = {
        "collections": {
            'match_pattern_collection': pattern_match_collection,
            'two_prompts_collection': two_prompts_collection
        }
    }

    ju.create_cfg('main_cfg', task_configuration)
    ju.task_full('task', 'main_cfg', with_show=True, profile_mode='df_show')


def run_pipelines():
    for d in os.listdir(data_folder):
        if d.startswith('dataframe'):
            match_prompt_selection_pipeline(os.path.join(data_folder, d),
                                            os.path.join(data_folder, 'two_prompts.csv'))


if __name__ == '__main__':
    run_pipelines()

